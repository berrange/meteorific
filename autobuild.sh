#!/bin/sh

set -e
set -v

rm -rf build vroot

meson --prefix="`pwd`/vroot" build

ninja -C build install

build-aux/make-dist
