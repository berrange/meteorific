/*
 * meteorific-analyser.h: FFT analysis of sample data
 *
 * Copyright (C) 2016 Daniel P. Berrange
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 */


#ifndef MET_ANALYSER_H__
#define MET_ANALYSER_H__

#include <glib-object.h>

#include "meteorific/meteorific-sample.h"
#include "meteorific/meteorific-spectrum.h"

#define MET_TYPE_ANALYSER            (met_analyser_get_type ())
#define MET_ANALYSER(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), MET_TYPE_ANALYSER, MetAnalyser))
#define MET_ANALYSER_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), MET_TYPE_ANALYSER, MetAnalyserClass))
#define MET_IS_ANALYSER(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), MET_TYPE_ANALYSER))
#define MET_IS_ANALYSER_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), MET_TYPE_ANALYSER))
#define MET_ANALYSER_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), MET_TYPE_ANALYSER, MetAnalyserClass))

typedef struct _MetAnalyser MetAnalyser;
typedef struct _MetAnalyserPrivate MetAnalyserPrivate;
typedef struct _MetAnalyserClass MetAnalyserClass;

struct _MetAnalyser
{
    GObject parent;

    MetAnalyserPrivate *priv;
};

struct _MetAnalyserClass
{
    GObjectClass parent_class;
};


GType met_analyser_get_type(void);

MetAnalyser *met_analyser_new(gint bins);


gboolean met_analyser_process(MetAnalyser *analyser,
			      MetSample *src,
			      MetSpectrum *dst,
			      GError **err);

#endif /* MET_ANALYSER_H__ */

/*
 * Local variables:
 *  c-indent-level: 4
 *  c-basic-offset: 4
 *  indent-tabs-mode: nil
 * End:
 */
