/*
 * meteorific-sample.h: sample data object
 *
 * Copyright (C) 2016 Daniel P. Berrange
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 */

#ifndef MET_SAMPLE_H__
#define MET_SAMPLE_H__

#include <glib-object.h>

#define MET_TYPE_SAMPLE            (met_sample_get_type ())

typedef struct _MetSample MetSample;

struct _MetSample
{
    guint8 *data;
    gsize len;
};

void met_sample_free(MetSample *sample);

GType met_sample_get_type(void);


MetSample *met_sample_new(guint32 len);
MetSample *met_sample_copy(MetSample *sample);
void met_sample_free(MetSample *sample);

#endif /* MET_SAMPLE_H__ */

/*
 * Local variables:
 *  c-indent-level: 4
 *  c-basic-offset: 4
 *  indent-tabs-mode: nil
 * End:
 */
