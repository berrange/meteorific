/*
 * meteorific-radio.h: radio dongle controller
 *
 * Copyright (C) 2016 Daniel P. Berrange
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 */


#ifndef MET_RADIO_H__
#define MET_RADIO_H__

#include <glib-object.h>

#include "meteorific/meteorific-sample.h"

#define MET_TYPE_RADIO            (met_radio_get_type ())
#define MET_RADIO(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), MET_TYPE_RADIO, MetRadio))
#define MET_RADIO_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), MET_TYPE_RADIO, MetRadioClass))
#define MET_IS_RADIO(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), MET_TYPE_RADIO))
#define MET_IS_RADIO_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), MET_TYPE_RADIO))
#define MET_RADIO_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), MET_TYPE_RADIO, MetRadioClass))

typedef struct _MetRadio MetRadio;
typedef struct _MetRadioPrivate MetRadioPrivate;
typedef struct _MetRadioClass MetRadioClass;

struct _MetRadio
{
    GObject parent;

    MetRadioPrivate *priv;
};

struct _MetRadioClass
{
    GObjectClass parent_class;
};


GType met_radio_get_type(void);

MetRadio *met_radio_new(void);

gboolean met_radio_open(MetRadio *radio, GError **err);
gboolean met_radio_close(MetRadio *radio, GError **err);

gboolean met_radio_set_freq_correction(MetRadio *radio, int ppm, GError **err);

gboolean met_radio_set_center_freq(MetRadio *radio, guint32 hz, GError **err);

gboolean met_radio_set_sample_rate(MetRadio *radio, guint32 hz, GError **err);

gboolean met_radio_set_auto_gain(MetRadio *radio, GError **err);

gboolean met_radio_set_manual_gain(MetRadio *radio, guint32 db, GError **err);

gboolean met_radio_read(MetRadio *radio, MetSample *dst, GError **err);

#endif /* MET_RADIO_H__ */

/*
 * Local variables:
 *  c-indent-level: 4
 *  c-basic-offset: 4
 *  indent-tabs-mode: nil
 * End:
 */
