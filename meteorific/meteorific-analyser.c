/*
 * meteorific-analyser.c: FFT analysis of sample data
 *
 * Copyright (C) 2016 Daniel P. Berrange
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 */

#include "meteorific/meteorific-analyser.h"

#include <complex.h>
#include <math.h>
#include <fftw3.h>
#include <string.h>

#define MET_ANALYSER_GET_PRIVATE(obj)                                   \
    (G_TYPE_INSTANCE_GET_PRIVATE((obj), MET_TYPE_ANALYSER, MetAnalyserPrivate))

struct _MetAnalyserPrivate
{
    fftwf_complex *input;
    fftwf_complex *output;
    fftwf_plan plan;

    gint bins;
};

G_DEFINE_TYPE_WITH_PRIVATE(MetAnalyser, met_analyser, G_TYPE_OBJECT);

#define MET_ANALYSER_ERROR met_analyser_error_quark ()

static GQuark met_analyser_error_quark(void)
{
    return g_quark_from_static_string("met-analyser-error");
}


enum {
    PROP_0,

    PROP_BINS,
};


static void
met_analyser_get_property(GObject*object,
			  guint prop_id,
			  GValue *value,
			  GParamSpec *pspec)
{
    MetAnalyser *analyser = MET_ANALYSER(object);
    MetAnalyserPrivate *priv = analyser->priv;

    switch (prop_id) {
    case PROP_BINS:
        g_value_set_int(value, priv->bins);
        break;
	
    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
        break;

    }
}


static void
met_analyser_set_property(GObject *object,
			  guint prop_id,
			  const GValue *value,
			  GParamSpec *pspec)
{
    MetAnalyser *analyser = MET_ANALYSER(object);
    MetAnalyserPrivate *priv = analyser->priv;

    switch (prop_id) {
    case PROP_BINS:
        priv->bins = g_value_get_int(value);
        break;
    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
        break;

    }
}



static void met_analyser_finalize(GObject *object)
{
    MetAnalyser *analyser = MET_ANALYSER(object);
    MetAnalyserPrivate *priv = analyser->priv;

    fftwf_destroy_plan(priv->plan);
    fftwf_free(priv->input);
    fftwf_free(priv->output);

    G_OBJECT_CLASS(met_analyser_parent_class)->finalize(object);
}


static GObject*
met_analyser_constructor(GType type,
			 guint n_construct_params,
			 GObjectConstructParam *construct_params)
{
    GObject *obj = G_OBJECT_CLASS(met_analyser_parent_class)->
        constructor(type,
                    n_construct_params,
                    construct_params);
    MetAnalyser *analyser = MET_ANALYSER(obj);
    MetAnalyserPrivate *priv = analyser->priv;

    priv->input = fftwf_alloc_complex(priv->bins);
    priv->output = fftwf_alloc_complex(priv->bins);
    priv->plan = fftwf_plan_dft_1d(priv->bins,
				   (fftwf_complex*)priv->input,
				   (fftwf_complex*)priv->output,
				   FFTW_FORWARD,
				   FFTW_MEASURE);

    return obj;
}

static void met_analyser_class_init(MetAnalyserClass *klass)
{
    GObjectClass *object_class = G_OBJECT_CLASS(klass);

    object_class->finalize = met_analyser_finalize;
    object_class->constructor = met_analyser_constructor;
    object_class->get_property = met_analyser_get_property;
    object_class->set_property = met_analyser_set_property;
    g_object_class_install_property(object_class,
                                    PROP_BINS,
                                    g_param_spec_int("bins",
                                                     "Bins",
                                                     "FFT bins",
                                                     0, 1 << 15, 2048,
                                                     G_PARAM_READWRITE |
                                                     G_PARAM_CONSTRUCT_ONLY |
                                                     G_PARAM_STATIC_NAME |
                                                     G_PARAM_STATIC_NICK |
                                                     G_PARAM_STATIC_BLURB));
}

static void met_analyser_init(MetAnalyser *analyser)
{
    analyser->priv = MET_ANALYSER_GET_PRIVATE(analyser);

}


MetAnalyser *met_analyser_new(gint bins)
{
    return MET_ANALYSER(g_object_new(MET_TYPE_ANALYSER,
                                     "bins", bins,
                                     NULL));
}


gboolean met_analyser_process(MetAnalyser *analyser,
			      MetSample *src,
			      MetSpectrum *dst,
			      GError **err)
{
    MetAnalyserPrivate *priv = analyser->priv;
    gsize sample, repeat, input;

    int nrepeats = src->len / dst->len / 2;

    #if 0
    if (src->len != (priv->bins * 2)) {
        g_set_error(err, MET_ANALYSER_ERROR, 0,
                    "Expected 4096 samples");
        return FALSE;
    }
    #endif
    if (dst->len != priv->bins) {
        g_set_error(err, MET_ANALYSER_ERROR, 0,
                    "Expected 4096 samples");
        return FALSE;
    }

    memset(dst->data, 0, sizeof(dst->data[0]) * dst->len);
    for (sample = 0, repeat = 0; repeat < nrepeats; repeat++) {
        for (input = 0; input < priv->bins; input++, sample+= 2) {
            float mult  = input % 2 == 0 ? 1 :-1;
            priv->input[input] = (CMPLXF(src->data[sample],
                                         src->data[sample + 1]) -
                                  CMPLXF(127.0, 127.0)) * mult;
        }

        fftwf_execute(priv->plan);

        for (input = 0; input < priv->bins; input++) {
            dst->data[input] += (powf(crealf(priv->output[input]), 2) +
                                 powf(cimag(priv->output[input]), 2));
        }
    }

    #if 0
    int size = priv->bins / 2;
    for (input = 0; input < size; input++) {
        float tmp = dst->data[input];
        dst->data[input] = dst->data[input + size];
        dst->data[input + size] = tmp;
    }
    #endif

    return TRUE;
}

/*
 * Local variables:
 *  c-indent-level: 4
 *  c-basic-offset: 4
 *  indent-tabs-mode: nil
 * End:
 */
