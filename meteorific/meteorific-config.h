/*
 * meteorific-config.h: capture and analysis configification
 *
 * Copyright (C) 2016 Daniel P. Berrange
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 */

#ifndef MET_CONFIG_H__
#define MET_CONFIG_H__

#include <glib-object.h>

#define MET_TYPE_CONFIG            (met_config_get_type ())

typedef struct _MetConfig MetConfig;

struct _MetConfig
{
  guint bins;
  guint manual_gain;
  gboolean auto_gain;
  guint sample_rate;
  guint center_frequency;
  guint ppm;
  guint integration;
  guint bandwidth;
};

void met_config_free(MetConfig *config);

GType met_config_get_type(void);

MetConfig *met_config_new(void);
MetConfig *met_config_copy(MetConfig *config);

void met_config_free(MetConfig *config);

#endif /* MET_CONFIG_H__ */

/*
 * Local variables:
 *  c-indent-level: 4
 *  c-basic-offset: 4
 *  indent-tabs-mode: nil
 * End:
 */
