/*
 * meteorific-display.c: radio capture display program
 *
 * Copyright (C) 2016 Daniel P. Berrange
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 */


#include "meteorific/meteorific-radio.h"
#include "meteorific/meteorific-analyser.h"
#include "meteorific/meteorific-config.h"

#include <sys/ioctl.h>
#include <stdio.h>
#include <unistd.h>
#include <math.h>
#include <gtk/gtk.h>
#include <string.h>

static const char *argv0;


typedef struct MetDisplay MetDisplay;
struct MetDisplay
{
    GtkWidget *window;
    GtkWidget *spectrum;
    GtkWidget *waterfall;
    GtkWidget *split;

    GMutex lock;
    MetConfig *config;
    gsize offset;

    cairo_surface_t *surface_spectrum;
    cairo_surface_t *surface_waterfall;
    gsize counter_waterfall;
};

static MetDisplay *display;

static gboolean spectrum_draw(GtkWidget *widget,
                              cairo_t *cr)
{
    double sx, sy;
    int ww, wh; /* Available drawing area extents */
    int pw, ph; /* Original image size */
    cairo_matrix_t m;

    ww = gdk_window_get_width(gtk_widget_get_window(widget));
    wh = gdk_window_get_height(gtk_widget_get_window(widget));
    pw = cairo_image_surface_get_width(display->surface_spectrum);
    ph = cairo_image_surface_get_height(display->surface_spectrum);

    sx = ww / (double)pw;
    sy = wh / (double)ph;
    g_mutex_lock(&display->lock);

    cairo_save(cr);
    cairo_get_matrix(cr, &m);
    cairo_scale(cr, sx, sy);

    cairo_set_source_surface(cr,
                             display->surface_spectrum,
                             0, 0);
    cairo_paint(cr);
    cairo_set_matrix(cr, &m);
    cairo_restore(cr);

    g_mutex_unlock(&display->lock);

    return TRUE;
}


static gboolean waterfall_draw(GtkWidget *widget,
                              cairo_t *cr)
{
    double sx, sy;
    int ww, wh; /* Available drawing area extents */
    int pw, ph; /* Original image size */
    cairo_matrix_t m;

    ww = gdk_window_get_width(gtk_widget_get_window(widget));
    wh = gdk_window_get_height(gtk_widget_get_window(widget));
    pw = cairo_image_surface_get_width(display->surface_waterfall);
    ph = cairo_image_surface_get_height(display->surface_waterfall);

    sx = ww / (double)pw;
    sy = wh / (double)ph;
    g_mutex_lock(&display->lock);

    cairo_save(cr);
    cairo_get_matrix(cr, &m);
    cairo_scale(cr, sx, sy);

    cairo_set_source_surface(cr,
                             display->surface_waterfall,
                             0, 0);
    cairo_paint(cr);
    cairo_set_matrix(cr, &m);


    cairo_set_source_rgb(cr, 0.3, 0, 0);
    cairo_move_to(cr, (ww / 2 - 5), wh);
    cairo_line_to(cr, (ww / 2), wh - 10);
    cairo_line_to(cr, (ww / 2 + 5), wh);
    cairo_line_to(cr, (ww / 2 - 5), wh);
    cairo_fill(cr);

    cairo_move_to(cr, (ww / 2 - 5), 0);
    cairo_line_to(cr, (ww / 2), 10);
    cairo_line_to(cr, (ww / 2 + 5), 0);
    cairo_line_to(cr, (ww / 2 - 5), 0);
    cairo_fill(cr);
   
    cairo_restore(cr);
    g_mutex_unlock(&display->lock);

    return TRUE;
}

static void render_spectrum(MetSpectrum *data, gfloat min, gfloat max)
{
    guint16 height = cairo_image_surface_get_height(display->surface_spectrum);
    guint16 width = cairo_image_surface_get_width(display->surface_spectrum);
    guint rowstride = cairo_image_surface_get_stride(display->surface_spectrum);
    guint8 *pixels = cairo_image_surface_get_data(display->surface_spectrum);
    int skip = (data->len - width) / 2;
    gsize i;

    g_assert(data->len >= width);

    g_mutex_lock(&display->lock);

    memset(pixels, 25, rowstride * height);

    float range = max - min;
    gsize j;

    for (i = 0 ; i < width ; i++) {
        float db = data->data[i + skip];

        int level = ((height - 1) * (db - min) / range);

        for (j = 0; j < level; j++) {
            int y = height - 1 - j;

            *(pixels + (y * rowstride) + (i * 4)) =0;
            *(pixels + (y * rowstride) + (i * 4) + 1) = 0;
            *(pixels + (y * rowstride) + (i * 4) + 2) = 100;
        }
    }

    g_mutex_unlock(&display->lock);
}


struct color {
    unsigned char r, g, b;
};
static const struct color waterfall[] = {
    { .r =   0, .g =   0, .b = 255 },
    { .r =   0, .g = 255, .b = 255 },
    { .r = 255, .g = 255, .b =   0 },
    { .r = 255, .g =   0, .b =   0 },
};

static float lerp(float x, float y0, float y1)
{

    return y0 + (y1 - y0) * x;
}

static void rgb_lerp(float x, struct color const *c0, struct color const *c1, struct color *r)
{
    r->r = floorf(lerp(x, c0->r, c1->r));
    r->g = floorf(lerp(x, c0->g, c1->g));
    r->b = floorf(lerp(x, c0->b, c1->b));

}

static gpointer save_waterfall(gpointer data)
{
    GdkPixbuf *pixbuf = data;
    GDateTime *now = g_date_time_new_now_utc();
    gchar *config = g_strdup_printf("freq=%u sample-rate=%u bins=%u gain=%u autogain=%d integration=%u bandwidth=%u ppm=%u",
                                  display->config->center_frequency,
                                  display->config->sample_rate,
                                  display->config->bins,
                                  display->config->manual_gain,
                                  display->config->auto_gain,
                                  display->config->integration,
                                  display->config->bandwidth,
                                  display->config->ppm);
    gchar *filename = g_strdup_printf("waterfall-%04d-%02d-%02d-%02d-%02d-%02d.png",
                                      g_date_time_get_year(now),
                                      g_date_time_get_month(now),
                                      g_date_time_get_day_of_month(now),
                                      g_date_time_get_hour(now),
                                      g_date_time_get_minute(now),
                                      g_date_time_get_second(now));
    GError *err = NULL;

    if (!gdk_pixbuf_save(pixbuf, filename, "png", &err,
                         "tEXt::key", config, NULL)) {
        g_printerr("Cannot save %s: %s\n", filename, err->message);
        g_error_free(err);
    }
    g_object_unref(pixbuf);
    g_free(filename);
    g_free(config);
    g_date_time_unref(now);

    return NULL;
}

static void render_waterfall(MetSpectrum *data, gfloat min, gfloat max)
{
    guint16 width = cairo_image_surface_get_width(display->surface_waterfall);
    guint16 height = cairo_image_surface_get_height(display->surface_waterfall);
    guint rowstride = cairo_image_surface_get_stride(display->surface_waterfall);
    guint8 *pixels = cairo_image_surface_get_data(display->surface_waterfall);
    int skip = (data->len - width) / 2;
    gsize i;

    g_assert(data->len >= width);

    g_mutex_lock(&display->lock);

    memmove(pixels, pixels + rowstride, rowstride * (height - 1));

    float gradient_width = 1.0 / (sizeof(waterfall)/sizeof(waterfall[0]) - 1.0);
    float range = max - min;
    pixels = pixels + (rowstride * (height - 1));

    for (i = 0 ; i < width ; i++) {
        struct color r;
        const struct color *col;
        float value = (data->data[i + skip] - min) / range;
        int pos = (int)(value / gradient_width);
        if (pos < 0)
            pos = 0;
        else if (pos > 3)
            pos = 3;
        const struct color *c1 = &(waterfall[pos]);
        const struct color *c2 = &(waterfall[pos + 1]);
        float x = (value - (pos * gradient_width)) / gradient_width;

        rgb_lerp(x, c1, c2, &r);
        col = &r;

        pixels[0] = col->r;
        pixels[1] = col->g;
        pixels[2] = col->b;
        pixels += 4;
    }

    display->counter_waterfall++;

    if (display->counter_waterfall == height) {
        display->counter_waterfall = 0;
        guint8 *duppixels = g_new0(guint8, height * rowstride);

        pixels = cairo_image_surface_get_data(display->surface_waterfall);
        memcpy(duppixels, pixels, height * rowstride);

        GdkPixbuf *pixbuf = gdk_pixbuf_get_from_surface(display->surface_waterfall,
                                                        0, 0, width, height);
        GThread *th = g_thread_new("image-save",
                                   save_waterfall,
                                   pixbuf);
        g_thread_unref(th);
    }

    g_mutex_unlock(&display->lock);
}


static gpointer analyser(gpointer data)
{
    MetConfig *config = data;
    MetRadio *radio;
    MetAnalyser *analyser;
    MetSample *input;
    MetSpectrum *output;
    GError *err = NULL;

    radio = met_radio_new();
    analyser = met_analyser_new(config->bins);

    input = met_sample_new(config->bins * 2 * config->integration);
    output = met_spectrum_new(config->bins);

    if (!met_radio_open(radio, &err))
        goto error;

    if (config->auto_gain) {
        if (!met_radio_set_auto_gain(radio, &err))
            goto error;
    } else {
        if (!met_radio_set_manual_gain(radio, config->manual_gain, &err))
            goto error;
    }

    if (!met_radio_set_center_freq(radio, config->center_frequency, &err))
        goto error;

    if (!met_radio_set_sample_rate(radio, config->sample_rate, &err))
        goto error;

    if (config->ppm && !met_radio_set_freq_correction(radio, config->ppm, &err))
        goto error;

    while (1) {
        if (!met_radio_read(radio, input, &err))
            goto error;

        if (!met_analyser_process(analyser, input, output, &err))
            goto error;

        gfloat min, max;
        met_spectrum_convert_to_db(output, config->integration, config->sample_rate,
                                   &min, &max);

        render_spectrum(output, min, max);
        render_waterfall(output, min, max);
    }

    if (!met_radio_close(radio, &err))
        goto error;

 cleanup:
    gtk_main_quit();
    return NULL;

 error:

    g_printerr("%s: %s\n", argv0, err->message);
    g_error_free(err);
    gtk_main_quit();
    goto cleanup;
}

static gboolean redraw(void)
{
    gtk_widget_queue_draw(display->spectrum);
    gtk_widget_queue_draw(display->waterfall);

    return TRUE;
}

int main(int argc, char **argv)
{
    GError *err = NULL;
    GOptionContext *context;
    MetConfig *config = met_config_new();
    GOptionEntry options [] = {
        { "bins", 'n', 0, G_OPTION_ARG_INT, &config->bins, "Perform FFT across N bins", "N" },
        { "frequency", 'f', 0, G_OPTION_ARG_INT, &config->center_frequency, "Center frequency HZ", "HZ" },
        { "sample-rate", 's', 0, G_OPTION_ARG_INT, &config->sample_rate, "Sample rate HZ", "HZ" },
        { "auto-gain", 'a', 0, G_OPTION_ARG_NONE, &config->auto_gain, "Automatic gain", NULL },
        { "gain", 'g', 0, G_OPTION_ARG_INT, &config->manual_gain, "Manual gain DB", "DB" },
        { "correction", 'c', 0, G_OPTION_ARG_INT, &config->ppm, "Frequency correction PPM", "PPM" },
        { "integration", 'i', 0, G_OPTION_ARG_INT, &config->integration, "Integrate sampling N times", "N" },
        { "bandwidth", 'b', 0, G_OPTION_ARG_INT, &config->bandwidth, "Display HZ bandwidth around center", "HZ" },
        { NULL, 0, 0, G_OPTION_ARG_NONE, NULL, NULL, 0 }
    };

    argv0 = argv[0];

    /* Setup command line options */
    context = g_option_context_new("");
    g_option_context_add_main_entries(context, options, NULL);
    g_option_context_add_group (context, gtk_get_option_group (TRUE));
    g_option_context_parse(context, &argc, &argv, &err);
    if (err) {
        g_print ("%s\n"
                 "Run '%s --help' to see a full list of available command line options\n",
                 err->message,
                 argv[0]);
        g_error_free(err);
        return 1;
    }

    if (config->bandwidth == 0)
        config->bandwidth = config->sample_rate;

    display = g_new0(MetDisplay, 1);

    display->window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    display->split = gtk_paned_new(GTK_ORIENTATION_VERTICAL);
    display->spectrum = gtk_drawing_area_new();
    display->waterfall = gtk_drawing_area_new();
    display->config = config;

    int subbins = (int)((double)config->bandwidth * (double)config->bins / (double)config->sample_rate);
    display->surface_spectrum = cairo_image_surface_create(CAIRO_FORMAT_RGB24, subbins, 300);

    display->surface_waterfall = cairo_image_surface_create(CAIRO_FORMAT_RGB24, subbins, 700);

    g_mutex_init(&display->lock);

    gtk_container_add(GTK_CONTAINER(display->window), display->split);
    gtk_paned_add1(GTK_PANED(display->split), display->spectrum);
    gtk_paned_add2(GTK_PANED(display->split), display->waterfall);

    g_signal_connect(display->spectrum, "draw", G_CALLBACK(spectrum_draw), NULL);
    g_signal_connect(display->waterfall, "draw", G_CALLBACK(waterfall_draw), NULL);

    g_timeout_add(50, (GSourceFunc)redraw, NULL);

    gtk_widget_set_size_request(display->spectrum, 1280, 200);
    gtk_widget_set_size_request(display->waterfall, 1280, 700);

    gtk_widget_show_all(display->window);

    g_thread_new("analyser",
                 analyser,
                 config);

    gtk_main();

    return 0;
}

/*
 * Local variables:
 *  c-indent-level: 4
 *  c-basic-offset: 4
 *  indent-tabs-mode: nil
 * End:
 */
