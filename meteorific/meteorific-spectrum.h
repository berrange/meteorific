/*
 * meteorific-spectrum.h: spectrum data object
 *
 * Copyright (C) 2016 Daniel P. Berrange
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 */

#ifndef MET_SPECTRUM_H__
#define MET_SPECTRUM_H__

#include <glib-object.h>

#define MET_TYPE_SPECTRUM            (met_spectrum_get_type ())

typedef struct _MetSpectrum MetSpectrum;

struct _MetSpectrum
{
    gfloat *data;
    gsize len;
};

void met_spectrum_free(MetSpectrum *spectrum);

GType met_spectrum_get_type(void);


MetSpectrum *met_spectrum_new(guint32 len);
MetSpectrum *met_spectrum_copy(MetSpectrum *spectrum);
void met_spectrum_free(MetSpectrum *spectrum);

void met_spectrum_convert_to_db(MetSpectrum *data,
                                int integration, int sample_rate,
                                float *min, float *max);

#endif /* MET_SPECTRUM_H__ */

/*
 * Local variables:
 *  c-indent-level: 4
 *  c-basic-offset: 4
 *  indent-tabs-mode: nil
 * End:
 */
