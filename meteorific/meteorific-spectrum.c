/*
 * meteorific-spectrum.c: spectrum data object
 *
 * Copyright (C) 2016 Daniel P. Berrange
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 */


#include "meteorific/meteorific-spectrum.h"

#include <string.h>
#include <math.h>


GType met_spectrum_get_type(void)
{
    static GType spectrum_type = 0;

    if (G_UNLIKELY(spectrum_type == 0)) {
        spectrum_type = g_boxed_type_register_static
            ("MetSpectrum",
             (GBoxedCopyFunc)met_spectrum_copy,
             (GBoxedFreeFunc)met_spectrum_free);
    }

    return spectrum_type;
}



MetSpectrum *met_spectrum_new(guint32 len)
{
    MetSpectrum *spectrum;

    spectrum = g_new0(MetSpectrum, 1);
    spectrum->len = len;
    spectrum->data = g_new0(gfloat, len);

    return spectrum;
}

MetSpectrum *met_spectrum_copy(MetSpectrum *spectrum)
{
    MetSpectrum *ret;

    ret = g_new0(MetSpectrum, 1);
    ret->data = g_new0(gfloat, spectrum->len);
    memcpy(ret->data, spectrum->data, spectrum->len);

    return ret;
}


void met_spectrum_free(MetSpectrum *spectrum)
{
    g_free(spectrum->data);
    g_free(spectrum);
}


void met_spectrum_convert_to_db(MetSpectrum *data,
                                int integration, int sample_rate,
                                float *min, float *max)
{
    gsize i;
    *min = *max = 0;

    data->data[data->len / 2] =
        (data->data[data->len / 2 - 1] +
         data->data[data->len / 2 + 1]) / 2;

    for (i = 0; i < data->len; i++) {
        data->data[i] = data->data[i] / (float)integration / (float)data->len / (float)sample_rate;
        data->data[i] = 20 * log10(data->data[i]);

        if (*max == 0 ||
            data->data[i] > *max)
            *max = data->data[i];
        if (*min == 0 ||
            data->data[i] < *min)
            *min = data->data[i];
    }

    if (0) {
        float range = *max - *min;
        for (i = 0; i < data->len; i++) {
            data->data[i] = (data->data[i] - *min) / range;
        }
    }
}


/*
 * Local variables:
 *  c-indent-level: 4
 *  c-basic-offset: 4
 *  indent-tabs-mode: nil
 * End:
 */
