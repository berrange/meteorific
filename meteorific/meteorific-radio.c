/*
 * meteorific-radio.c: radio dongle controller
 *
 * Copyright (C) 2016 Daniel P. Berrange
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 */

#include "meteorific/meteorific-radio.h"

#include <rtl-sdr.h>

#define MET_RADIO_GET_PRIVATE(obj)                                      \
    (G_TYPE_INSTANCE_GET_PRIVATE((obj), MET_TYPE_RADIO, MetRadioPrivate))

struct _MetRadioPrivate
{
    rtlsdr_dev_t *dev;
};

G_DEFINE_TYPE_WITH_PRIVATE(MetRadio, met_radio, G_TYPE_OBJECT);

#define MET_RADIO_ERROR met_radio_error_quark ()

static GQuark met_radio_error_quark(void)
{
    return g_quark_from_static_string("met-radio-error");
}

static void met_radio_finalize(GObject *object)
{
    MetRadio *radio = MET_RADIO(object);
    MetRadioPrivate *priv = radio->priv;

    if (priv->dev)
        rtlsdr_close(priv->dev);
    
    G_OBJECT_CLASS(met_radio_parent_class)->finalize(object);
}

static void met_radio_class_init(MetRadioClass *klass)
{
    GObjectClass *object_class = G_OBJECT_CLASS(klass);

    object_class->finalize = met_radio_finalize;
}

static void met_radio_init(MetRadio *radio)
{
    radio->priv = MET_RADIO_GET_PRIVATE(radio);
}


MetRadio *met_radio_new(void)
{
    return MET_RADIO(g_object_new(MET_TYPE_RADIO, NULL));
}


gboolean met_radio_open(MetRadio *radio, GError **err)
{
    MetRadioPrivate *priv = radio->priv;
    if (priv->dev) {
        g_set_error(err, MET_RADIO_ERROR, 0,
                    "Device is already open");
        return FALSE;
    }


    if (rtlsdr_open(&priv->dev, 0) < 0) {
        g_set_error(err, MET_RADIO_ERROR, 0,
                    "Unable to open radio");
        return FALSE;
    }
  
    return TRUE;
}

gboolean met_radio_close(MetRadio *radio, GError **err)
{
    MetRadioPrivate *priv = radio->priv;
    if (!priv->dev) {
        g_set_error(err, MET_RADIO_ERROR, 0,
                    "Device is not open");
        return FALSE;
    }

    rtlsdr_close(priv->dev);
    priv->dev = NULL;

    return TRUE;
}

gboolean met_radio_set_freq_correction(MetRadio *radio, int ppm, GError **err)
{
    MetRadioPrivate *priv = radio->priv;

    if (!priv->dev) {
        g_set_error(err, MET_RADIO_ERROR, 0,
                    "Device is not open");
        return FALSE;
    }

    if (rtlsdr_set_freq_correction(priv->dev, ppm) < 0) {
        g_set_error(err, MET_RADIO_ERROR, 0,
                    "Unable to set PPM correction");
        return FALSE;
    }

    return TRUE;
}


gboolean met_radio_set_center_freq(MetRadio *radio, guint32 hz, GError **err)
{
    MetRadioPrivate *priv = radio->priv;
    if (!priv->dev) {
        g_set_error(err, MET_RADIO_ERROR, 0,
                    "Device is not open");
        return FALSE;
    }

    if (rtlsdr_set_center_freq(priv->dev, hz) < 0) {
        g_set_error(err, MET_RADIO_ERROR, 0,
                    "Unable to set PPM correction");
        return FALSE;
    }

    return TRUE;
}


gboolean met_radio_set_sample_rate(MetRadio *radio, guint32 hz, GError **err)
{
    MetRadioPrivate *priv = radio->priv;
    if (!priv->dev) {
        g_set_error(err, MET_RADIO_ERROR, 0,
                    "Device is not open");
        return FALSE;
    }

    if (rtlsdr_set_sample_rate(priv->dev, hz) < 0) {
        g_set_error(err, MET_RADIO_ERROR, 0,
                    "Unable to set sample rate");
        return FALSE;
    }

    return TRUE;
}

gboolean met_radio_set_auto_gain(MetRadio *radio, GError **err)
{
    MetRadioPrivate *priv = radio->priv;
    if (!priv->dev) {
        g_set_error(err, MET_RADIO_ERROR, 0,
                    "Device is not open");
        return FALSE;
    }

    if (rtlsdr_set_tuner_gain_mode(priv->dev, 0) < 0) {
        g_set_error(err, MET_RADIO_ERROR, 0,
                    "Cannot switch to auto gain");
        return FALSE;
    }

    return TRUE;
}

gboolean met_radio_set_manual_gain(MetRadio *radio, guint32 db, GError **err)
{
    MetRadioPrivate *priv = radio->priv;
    if (!priv->dev) {
        g_set_error(err, MET_RADIO_ERROR, 0,
                    "Device is not open");
        return FALSE;
    }

    if (rtlsdr_set_tuner_gain_mode(priv->dev, 1) < 0) {
        g_set_error(err, MET_RADIO_ERROR, 0,
                    "Cannot switch to manual gain");
        return FALSE;
    }

    if (rtlsdr_set_tuner_gain(priv->dev, db) < 0) {
        g_set_error(err, MET_RADIO_ERROR, 0,
                    "Cannot switch to manual gain");
        return FALSE;
    }

    return TRUE;
}



gboolean met_radio_read(MetRadio *radio, MetSample *dst, GError **err)
{
    MetRadioPrivate *priv = radio->priv;
    int nread;

    if (!priv->dev) {
        g_set_error(err, MET_RADIO_ERROR, 0,
                    "Device is not open");
        return FALSE;
    }

    if (rtlsdr_reset_buffer(priv->dev) < 0) {
        g_set_error(err, MET_RADIO_ERROR, 0,
                    "Cannot reset device buffer");
        return FALSE;
    }

    if (rtlsdr_read_sync(priv->dev, dst->data, dst->len, &nread) < 0) {
        g_set_error(err, MET_RADIO_ERROR, 0,
                    "Cannot read sample");
        return FALSE;
    }

    if (nread != dst->len) {
        g_set_error(err, MET_RADIO_ERROR, 0,
                    "Got short sample");
        return FALSE;
    }

    return TRUE;
}

/*
 * Local variables:
 *  c-indent-level: 4
 *  c-basic-offset: 4
 *  indent-tabs-mode: nil
 * End:
 */
