/*
 * meteorific.c: radio capture CLI program
 *
 * Copyright (C) 2016 Daniel P. Berrange
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 */

#include "meteorific/meteorific-radio.h"
#include "meteorific/meteorific-analyser.h"

#include <sys/ioctl.h>
#include <stdio.h>
#include <unistd.h>
#include <math.h>
#include <gdk-pixbuf/gdk-pixbuf.h>


static gboolean scale = TRUE;

static void powerToDB(MetSpectrum *data,
                      int integration, int bandwidth,
                      float *min, float *max)
{
    gsize i;
    *min = *max = 0;

    data->data[data->len / 2] =
        (data->data[data->len / 2 - 1] +
         data->data[data->len / 2 + 1]) / 2;

    for (i = 0; i < data->len; i++) {
        data->data[i] = data->data[i] / (float)integration / (float)data->len / (float)bandwidth;
        data->data[i] = 20 * log10(data->data[i]);

        if (*max == 0 ||
            data->data[i] > *max)
            *max = data->data[i];
        if (*min == 0 ||
            data->data[i] < *min)
            *min = data->data[i];
    }

    if (scale) {
        float range = *max - *min;
        for (i = 0; i < data->len; i++) {
            data->data[i] = (data->data[i] - *min) / range;
        }
    }
}


static void display(MetSpectrum *data, int bandwidth, int integration)
{
    gsize i, j;
    float min = 0, max = 0;
    gsize nscreen;
    char *screen;

    powerToDB(data, integration, bandwidth, &min, &max);

    for (i =0 ; i < data->len ; i++) {
        float db = data->data[i] * -1;

        if (db > max)
            max = db;
    }

    struct winsize w;
    ioctl(STDOUT_FILENO, TIOCGWINSZ, &w);
    //    max = 302068704.000000;
    nscreen = w.ws_row * w.ws_col;
    screen = g_new0(char, nscreen);

    printf("\e[1;1H\e[2J");

    for (i =0 ; i < data->len ; i++) {
        float db = data->data[i] * -1;

        int y = (int)(w.ws_row * db / max);
        int x = (int)(w.ws_col * i / data->len);

        if (y > w.ws_row)
            y = w.ws_row;

        //screen[((w.ws_row - 1 - y) * w.ws_col) + x] = '#';
        for (j = y; j < w.ws_col; j++) {
            printf("\e[%d;%dH#", (int)( j),  x);
        }
    }

    //    write(STDOUT_FILENO, screen, nscreen);
    fflush(stdout);
    usleep(100*1000);
    g_free(screen);
}


static void plot(MetSpectrum *data, int freq, int bandwidth, int integration)
{
    gsize i;
    float min, max;

    powerToDB(data, integration, bandwidth, &min, &max);

    //    g_print("plot \"-\" smooth csplines\n");
    g_print("plot \"-\" with lines\n");
    for (i = 0; i < data->len; i++) {
        double offset = (double)freq + ((((double)i - (double)data->len / 2.0) * (double)bandwidth / (double)data->len));
        g_print("%f %.03f\n", offset / 1000000.0, (double)data->data[i]);
    }
    g_print("e\n");
}


struct color {
    unsigned char r, g, b;
};
static const struct color waterfall[] = {
    { .r =   0, .g =   0, .b = 255 },
    { .r =   0, .g = 255, .b = 255 },
    { .r = 255, .g = 255, .b =   0 },
    { .r = 255, .g =   0, .b =   0 },
};

static float lerp(float x, float y0, float y1)
{

    return y0 + (y1 - y0) * x;
}

static void rgb_lerp(float x, struct color const *c0, struct color const *c1, struct color *r)
{
    r->r = floorf(lerp(x, c0->r, c1->r));
    r->g = floorf(lerp(x, c0->g, c1->g));
    r->b = floorf(lerp(x, c0->b, c1->b));

}

static void render(GdkPixbuf *pixbuf, MetSpectrum *data, int repeat, int bandwidth, int integration)
{
    gsize i;
    float min = 0, max = 0;

    double gradient_width = 1.0 / (sizeof(waterfall)/sizeof(waterfall[0]) - 1.0);

    powerToDB(data, integration, bandwidth, &min, &max);

    for (i = 0; i < data->len; i++) {
        struct color r;
        struct color const *col;
        double value = data->data[i];

        int pos = (int)(value / gradient_width);
        if (pos < 0)
            pos = 0;
        else if (pos > 3)
            pos = 3;
        const struct color *c1 = &(waterfall[pos]);
        const struct color *c2 = &(waterfall[pos + 1]);
        float x = (value - (pos * gradient_width)) / gradient_width;


        rgb_lerp(x, c1, c2, &r);
        col = &r;

        guchar *pixels,*p;
        int rowstride;
        int n_channels;
        n_channels = gdk_pixbuf_get_n_channels(pixbuf);
        rowstride = gdk_pixbuf_get_rowstride(pixbuf);
        pixels = gdk_pixbuf_get_pixels(pixbuf);
        p = pixels + repeat * rowstride + i * n_channels;
        p[0] = col->r;
        p[1] = col->g;
        p[2] = col->b;
    }
}

static void print(MetSpectrum *data, guint64 now, int freq, int bandwidth, int integration)
{
    gsize i;
    gfloat min, max;
    #if 0
    g_print("set dgrid3d 30,30\n");
    g_print("set hidden3d\n");
    g_print("splot \"-\" u 1:2:3 with lines\n");
    #endif

    powerToDB(data, integration, bandwidth, &min, &max);
    for (i = 0; i < data->len; i++) {
        double offset = (double)freq + (((i - data->len / 2.0) * (double)bandwidth / data->len));
        g_print("%llu %f %f\n", (unsigned long long) now, offset, (double)data->data[i]);
    }
    //    g_print("\n");
}

int main(int argc, char **argv)
{
    GError *err = NULL;
    MetRadio *radio;
    MetAnalyser *analyser;
    MetSample *input;
    MetSpectrum *output;
    int bins = 2048;
    int manual_gain = 0;
    gboolean auto_gain = FALSE;
    int bandwidth = 1000000;
    int frequency = 143050000;
    int ppm = 0;
    int integration = 10;
    int repeats = 1, repeat = 0;
    GOptionContext *context;
    GOptionEntry options [] = {
        { "bins", 'n', 0, G_OPTION_ARG_INT, &bins, "Perform FFT across N bins", "N" },
        { "frequency", 'f', 0, G_OPTION_ARG_INT, &frequency, "Center frequency HZ", "HZ" },
        { "bandwidth", 'b', 0, G_OPTION_ARG_INT, &bandwidth, "Bandwidth HZ", "HZ" },
        { "auto-gain", 'a', 0, G_OPTION_ARG_NONE, &auto_gain, "Automatic gain", NULL },
        { "gain", 'g', 0, G_OPTION_ARG_INT, &manual_gain, "Manual gain DB", "DB" },
        { "correction", 'c', 0, G_OPTION_ARG_INT, &ppm, "Frequency correction PPM", "PPM" },
        { "integration", 'i', 0, G_OPTION_ARG_INT, &integration, "Integrate sampling N times", "N" },
        { "repeats", 'r', 0, G_OPTION_ARG_INT, &repeats, "Repeat sampling N times", "N" },
        { NULL, 0, 0, G_OPTION_ARG_NONE, NULL, NULL, 0 }
    };
    guint64 now;
    GdkPixbuf *pixbuf;

    /* Setup command line options */
    context = g_option_context_new("");
    g_option_context_add_main_entries(context, options, NULL);
    g_option_context_parse(context, &argc, &argv, &err);
    if (err) {
        g_print ("%s\n"
                 "Run '%s --help' to see a full list of available command line options\n",
                 err->message,
                 argv[0]);
        g_error_free(err);
        return 1;
    }

    radio = met_radio_new();
    analyser = met_analyser_new(bins);

    input = met_sample_new(bins * 2 * integration);
    output = met_spectrum_new(bins);

    if (!met_radio_open(radio, &err))
        goto error;

    if (auto_gain) {
        if (!met_radio_set_auto_gain(radio, &err))
            goto error;
    } else if (manual_gain) {
        if (!met_radio_set_manual_gain(radio, manual_gain, &err))
            goto error;
    }

    if (frequency &&
        !met_radio_set_center_freq(radio, frequency, &err))
        goto error;

    if (bandwidth &&
        !met_radio_set_sample_rate(radio, bandwidth, &err))
        goto error;

    if (ppm &&
        !met_radio_set_freq_correction(radio, ppm, &err))
        goto error;


    pixbuf = gdk_pixbuf_new(GDK_COLORSPACE_RGB,
                            FALSE,
                            8,
                            bins,
                            repeats);
    while (repeat < repeats) {
        now = g_get_real_time();
        if (!met_radio_read(radio, input, &err))
            goto error;

        if (!met_analyser_process(analyser, input, output, &err))
            goto error;

        if (0)
            plot(output, frequency, bandwidth, integration);
        else if (0)
            display(output, bandwidth, integration);
        else if (0)
            print(output, now, frequency, bandwidth, integration);
        else
            render(pixbuf, output, repeat, bandwidth, integration);

        repeat++;
    }

    if (!met_radio_close(radio, &err))
        goto error;

    gdk_pixbuf_save(pixbuf, "spectrum.png", "png", NULL, NULL);

    return 0;

 error:
    g_printerr("%s: %s\n", argv[0], err->message);
    g_error_free(err);
    return 1;
}

/*
 * Local variables:
 *  c-indent-level: 4
 *  c-basic-offset: 4
 *  indent-tabs-mode: nil
 * End:
 */
