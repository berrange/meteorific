/*
 * meteorific-config.c: capture and analysis configification
 *
 * Copyright (C) 2016 Daniel P. Berrange
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 */


#include "meteorific/meteorific-config.h"

#include <string.h>

GType met_config_get_type(void)
{
    static GType config_type = 0;

    if (G_UNLIKELY(config_type == 0)) {
        config_type = g_boxed_type_register_static
            ("MetConfig",
             (GBoxedCopyFunc)met_config_copy,
             (GBoxedFreeFunc)met_config_free);
    }

    return config_type;
}



MetConfig *met_config_new(void)
{
    MetConfig *config;

    config = g_new0(MetConfig, 1);
    config->bins = 2048;
    config->manual_gain = 278;
    config->auto_gain = FALSE;
    config->sample_rate = 250000;
    config->center_frequency = 143050000;
    //config->center_frequency = 92050000;
    config->ppm = 0;
    config->integration = 10;

    config->bandwidth = 0;

    return config;
}

MetConfig *met_config_copy(MetConfig *config)
{
    MetConfig *ret;

    ret = g_new0(MetConfig, 1);
    memcpy(ret, config, sizeof(*config));

    return ret;
}


void met_config_free(MetConfig *config)
{
    g_free(config);
}

/*
 * Local variables:
 *  c-indent-level: 4
 *  c-basic-offset: 4
 *  indent-tabs-mode: nil
 * End:
 */
