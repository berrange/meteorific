/*
 * meteorific-sample.c: sample data object
 *
 * Copyright (C) 2016 Daniel P. Berrange
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 */


#include "meteorific/meteorific-sample.h"

#include <string.h>

GType met_sample_get_type(void)
{
    static GType sample_type = 0;

    if (G_UNLIKELY(sample_type == 0)) {
        sample_type = g_boxed_type_register_static
            ("MetSample",
             (GBoxedCopyFunc)met_sample_copy,
             (GBoxedFreeFunc)met_sample_free);
    }

    return sample_type;
}



MetSample *met_sample_new(guint32 len)
{
    MetSample *sample;

    sample = g_new0(MetSample, 1);
    sample->len = len;
    sample->data = g_new0(guint8, len);

    return sample;
}

MetSample *met_sample_copy(MetSample *sample)
{
    MetSample *ret;

    ret = g_new0(MetSample, 1);
    ret->data = g_new0(guint8, sample->len);
    memcpy(ret->data, sample->data, sample->len);

    return ret;
}


void met_sample_free(MetSample *sample)
{
    g_free(sample->data);
    g_free(sample);
}

/*
 * Local variables:
 *  c-indent-level: 4
 *  c-basic-offset: 4
 *  indent-tabs-mode: nil
 * End:
 */
